import { createElement, useEffect, useState } from "react";
import Layout from "antd/es/layout";
import Menu from "antd/es/menu";
import Dropdown from "antd/es/dropdown";
import Modal from "antd/es/modal";
import Button from "antd/es/button";
import Avatar from "antd/es/avatar";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  LogoutOutlined,
  HomeOutlined,
  ShopOutlined,
  ExclamationCircleOutlined,
  UserOutlined,
  LockOutlined,
  DownOutlined,
  ContainerOutlined,
} from "@ant-design/icons";
import "./MainLayout.css";
import logoWhite from "../assets/img/logo.png";
import helper from "../utils/helper";
import history from "../utils/history";
import http from "../utils/http";
import ResetForm from "../components/Forms/ResetForm";

const getIsMobile = () => window.innerWidth <= 768;

function useIsMobile() {
  const [isMobile, setIsMobile] = useState(getIsMobile());

  useEffect(() => {
    const onResize = () => {
      setIsMobile(getIsMobile());
    };

    window.addEventListener("resize", onResize);

    return () => {
      window.removeEventListener("resize", onResize);
    };
  }, []);

  return isMobile;
}
function MainLayout(props) {
  const isMobile = useIsMobile();
  const [collapsed, setCollapsed] = useState(false);
  const [visiblePassword, setVisiblePassword] = useState(false);
  const { Header, Sider, Content } = Layout;
  const { children, title } = props;

  useEffect(() => {
    if (isMobile) {
      setCollapsed(true);
    } else {
      setCollapsed(false);
    }
  }, [isMobile]);

  return (
    <Layout>
      <Sider
        theme="light"
        trigger={null}
        collapsible
        collapsed={collapsed}
        collapsedWidth={isMobile ? 0 : 80}
        breakpoint="lg"
        style={{ overflowY: "auto", height: window.innerHeight }}
      >
        <div className="logo">
          <img src={logoWhite} alt="Logo" />
        </div>
        <Menu
          theme="light"
          mode="inline"
          defaultSelectedKeys={[window.location.pathname]}
          onClick={(e) => {
            if (e.key === "/logout") {
              http.get("/v1/logout").then((res) => {
                if (res) {
                  localStorage.removeItem("db");
                  window.location.reload(true);
                }
              });
            } else {
              history.push(e.key);
            }
          }}
        >
          <Menu.Item key="/dashboard" icon={<HomeOutlined />}>
            Dashboard
          </Menu.Item>
          <Menu.Item key="/product" icon={<ContainerOutlined />}>
            Produk
          </Menu.Item>
          <Menu.Item key="/transaction" icon={<ShopOutlined />}>
            Transaksi
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background flex align-center"
          style={{ padding: 0 }}
        >
          {createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: "trigger",
            onClick: () => setCollapsed(!collapsed),
          })}
          <span
            style={{
              marginLeft: !isMobile ? 0 : "unset",
              fontSize: !isMobile ? 17 : 15,
              marginRight: 5,
            }}
          >
            {helper.truncString(title, 30, "...")}
          </span>
          {(collapsed && isMobile) || !isMobile ? (
            <div
              style={{ marginLeft: "auto", marginRight: 30, cursor: "pointer" }}
            >
              <Dropdown
                overlay={
                  <Menu
                    onClick={(e) => {
                      if (e.key === "/logout") {
                        Modal.confirm({
                          title: "Logout",
                          icon: <ExclamationCircleOutlined />,
                          content: "Anda yakin ingin logout?",
                          okText: "Ya",
                          cancelText: "Tidak",
                          onOk: () => {
                            http.get("/v1/logout").then((res) => {
                              if (res) {
                                localStorage.removeItem("db");
                                window.location.reload(true);
                              }
                            });
                          },
                        });
                      } else if (e.key === "/reset-password") {
                        setVisiblePassword(true);
                      } else {
                        window.location.href = e.key;
                      }
                    }}
                  >
                    <Menu.Item disabled key={"/info"}>
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <Avatar icon={<UserOutlined />} />
                        <div
                          style={{
                            marginLeft: 10,
                            display: "flex",
                            flexDirection: "column",
                            color: "#294354",
                            fontWeight: "bold",
                          }}
                        >
                          {helper.isAuthenticated()?.user?.name}
                          <span
                            style={{
                              color: "rgb(161, 165, 183)",
                              fontSize: 12,
                              marginTop: "-5px",
                              fontWeight: "normal",
                            }}
                          >
                            {helper.isAuthenticated()?.user?.email}
                          </span>
                        </div>
                      </div>
                    </Menu.Item>
                    <Menu.Divider />
                    <Menu.Item key="/reset-password" icon={<LockOutlined />}>
                      Ubah password
                    </Menu.Item>
                    <Menu.Divider />
                    <Menu.Item key="/logout" icon={<LogoutOutlined />}>
                      Logout
                    </Menu.Item>
                  </Menu>
                }
                trigger={["click"]}
              >
                <a
                  href="/#"
                  className="ant-dropdown-link"
                  onClick={(e) => e.preventDefault()}
                >
                  <Avatar icon={<UserOutlined />} />
                  <DownOutlined
                    style={{
                      fontSize: 10,
                      marginLeft: 10,
                      fontWeight: "bold",
                      color: "#aaa",
                    }}
                  />
                </a>
              </Dropdown>
            </div>
          ) : null}
        </Header>
        <Content
          className="site-layout-background"
          style={{
            minHeight: 280,
            background: "unset",
            padding: props.padding,
          }}
        >
          {children}
        </Content>
        <Modal
          centered
          visible={visiblePassword}
          title={"Reset password"}
          onCancel={async () => {
            await setVisiblePassword(false);
          }}
          footer={[
            <Button
              key="back"
              onClick={async () => {
                await setVisiblePassword(false);
              }}
            >
              Tutup
            </Button>,
          ]}
        >
          <ResetForm
            handleClose={async () => {
              await setVisiblePassword(false);
            }}
          />
        </Modal>
      </Layout>
    </Layout>
  );
}
export default MainLayout;
