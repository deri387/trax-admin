import Card from "antd/es/card";
import Image from "antd/es/image";
import logo from "../assets/img/logo.png";
import "./FullLayout.css";
function FullLayout(props) {
  const { children, title } = props;
  return (
    <div className="full-layout">
      <div className="middle">
        <Card bordered={false} style={{ width: 350 }}>
          <Image
            src={logo}
            width="100px"
            style={{ marginBottom: 30 }}
            preview={false}
          />{" "}
          <br />
          <span
            style={{
              fontSize: 15,
              color: "var(--ant-primary-color)",
              textTransform: "uppercase",
              fontWeight: "bold",
            }}
          >
            {title}
          </span>
          <div style={{ marginTop: 15 }}>{children}</div>
        </Card>
      </div>
    </div>
  );
}
export default FullLayout;
