import TableProduct from "../components/Tables/TableProduct";
import MainLayout from "../layouts/MainLayout";

function ProductListPage() {
  return (
    <MainLayout title="Produk">
      <div style={{ padding: 20 }}>
        <TableProduct />
      </div>
    </MainLayout>
  );
}
export default ProductListPage;
