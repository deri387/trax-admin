import Card from "antd/es/card";
import ProductForm from "../components/Forms/ProductForm";
import MainLayout from "../layouts/MainLayout";

function ProductAddPage() {
  return (
    <MainLayout title="Tambah Produk">
      <div style={{ overflowY: "auto", height: window.innerHeight }}>
        <Card className="card-form">
          <ProductForm />
        </Card>
      </div>
    </MainLayout>
  );
}
export default ProductAddPage;
