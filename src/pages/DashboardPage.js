import Row from "antd/es/row";
import Col from "antd/es/col";
import Statistic from "antd/es/statistic";
import Card from "antd/es/card";
import { UserOutlined, TagOutlined, ShopOutlined } from "@ant-design/icons";
import MainLayout from "../layouts/MainLayout";
import TableTransaction from "../components/Tables/TableTransaction";

function DashboardPage() {
  return (
    <MainLayout title="Dashboard">
      <div style={{ padding: 20 }}>
        <Row gutter={[12, 12]}>
          <Col
            xs={{ span: 24 }}
            sm={{ span: 24 }}
            md={{ span: 24 }}
            lg={{ span: 8 }}
          >
            <Card>
              <Statistic
                title="User"
                value={10}
                prefix={
                  <UserOutlined style={{ color: "rgba(253, 107, 187, 1)" }} />
                }
              />
            </Card>
          </Col>
          <Col
            xs={{ span: 24 }}
            sm={{ span: 24 }}
            md={{ span: 24 }}
            lg={{ span: 8 }}
          >
            <Card>
              <Statistic
                title="Produk"
                value={10}
                prefix={<TagOutlined style={{ color: "#1890ff" }} />}
              />
            </Card>
          </Col>
          <Col
            xs={{ span: 12 }}
            sm={{ span: 12 }}
            md={{ span: 12 }}
            lg={{ span: 8 }}
          >
            <Card>
              <Statistic
                title="Transaksi"
                value={10}
                prefix={
                  <ShopOutlined style={{ color: "rgba(255, 122, 0, 1)" }} />
                }
              />
            </Card>
          </Col>
        </Row>
        <Card title={"5 Transaksi Terakhir"} style={{ marginTop: 10 }}>
          <TableTransaction action={false} />
        </Card>
      </div>
    </MainLayout>
  );
}
export default DashboardPage;
