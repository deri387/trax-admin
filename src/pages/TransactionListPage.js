import TableTransaction from "../components/Tables/TableTransaction";
import MainLayout from "../layouts/MainLayout";

function TransactionListPage() {
  return (
    <MainLayout title="Transaksi">
      <div style={{ padding: 20 }}>
        <TableTransaction action={true} />
      </div>
    </MainLayout>
  );
}
export default TransactionListPage;
