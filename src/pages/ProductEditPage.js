import Card from "antd/es/card";
import Result from "antd/es/result";
import Button from "antd/es/button";
import { useEffect, useState } from "react";
import ProductForm from "../components/Forms/ProductForm";
import MainLayout from "../layouts/MainLayout";
import http from "../utils/http";

function ProductEditPage(props) {
  const [retrieve, setRetrieve] = useState(null);
  useEffect(() => {
    http.post("v1/product/retrieve", { id: props.id }).then((res) => {
      if (res) {
        setRetrieve(res.data.serve);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <MainLayout title="Edit Produk">
      <div style={{ overflowY: "auto", height: window.innerHeight }}>
        <Card className="card-form">
          {retrieve ? (
            <ProductForm data={retrieve} />
          ) : (
            <Result
              status="500"
              title="500"
              subTitle="Terjadi kesalahan, silahkan coba kembali lagi nanti."
              extra={
                <Button type="primary" onClick={() => window.history.back()}>
                  Kembali
                </Button>
              }
            />
          )}
        </Card>
      </div>
    </MainLayout>
  );
}
export default ProductEditPage;
