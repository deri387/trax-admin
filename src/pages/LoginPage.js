import LoginForm from "../components/Forms/LoginForm";
import FullLayout from "../layouts/FullLayout";

function LoginPage() {
  return (
    <FullLayout title="Login">
      <LoginForm />
    </FullLayout>
  );
}
export default LoginPage;
