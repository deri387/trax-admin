import Form from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import { InboxOutlined, LockOutlined } from "@ant-design/icons";
import http from "../../utils/http";

function LoginForm() {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    http.post("/v1/auth/login/root", values).then((res) => {
      if (res) {
        localStorage.setItem("db", JSON.stringify(res.data.serve));
        window.location.reload(true);
      }
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Form
      form={form}
      name="basic"
      layout="vertical"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="Email"
        name="email"
        rules={[
          { required: true, message: "Email wajib diisi" },
          { type: "email", message: "Email tidak valid" },
        ]}
        hasFeedback
      >
        <Input prefix={<InboxOutlined />} placeholder="Masukkan email Anda" />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true, message: "Password wajib diisi!" }]}
        hasFeedback
      >
        <Input.Password
          prefix={<LockOutlined />}
          placeholder="Masukkan password Anda"
        />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="float-left">
          Login
        </Button>
      </Form.Item>
    </Form>
  );
}
export default LoginForm;
