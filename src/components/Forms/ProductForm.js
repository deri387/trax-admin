import Form from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import Row from "antd/es/row";
import Col from "antd/es/col";
import Popconfirm from "antd/es/popconfirm";
import PageHeader from "antd/es/page-header";
import { useEffect } from "react";
import http from "../../utils/http";
import history from "../../utils/history";

function ProductForm(props) {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    if (props.data) {
      http.put("/v1/product", values).then(async (res) => {
        if (res) {
          history.goBack();
        }
      });
    } else {
      http.post("/v1/product", values).then(async (res) => {
        if (res) {
          history.goBack();
        }
      });
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const init = {
    id: props.data ? props.data.id : "",
    name: props.data ? props.data.name : "",
    stock: props.data ? props.data.stock : "",
    price: props.data ? props.data.price : "",
    price_disc: props.data ? props.data.price_disc : "",
    price_total: "",
  };
  useEffect(() => {
    form.setFieldsValue(init);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.data]);

  return (
    <Form
      form={form}
      name="basic"
      layout="vertical"
      initialValues={init}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item label="ID" name="id" hasFeedback hidden>
        <Input type="hidden" />
      </Form.Item>
      <PageHeader
        ghost={false}
        onBack={() => window.history.back()}
        title="Form Produk"
        subTitle=""
        extra={[
          props.data ? (
            <Popconfirm
              key={"delete"}
              placement="left"
              title="Anda yakin ingin menghapus data ini?"
              onConfirm={() => {
                const postData = { id: props.data.id };
                http({
                  url: "/v1/product",
                  method: "DELETE",
                  data: postData,
                }).then(() => history.goBack());
              }}
              okText="Iya"
              cancelText="Batal"
            >
              <Button type="danger" shape="round">
                Hapus
              </Button>
            </Popconfirm>
          ) : null,
        ]}
      ></PageHeader>
      <div style={{ padding: "10px 20px 20px 20px" }}>
        <Form.Item
          label="Nama produk"
          name="name"
          rules={[{ required: true, message: "Nama produk wajib diisi" }]}
          hasFeedback
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Stok"
          name="stock"
          rules={[{ required: true, message: "Stok produk wajib diisi" }]}
          hasFeedback
        >
          <Input type={"number"} />
        </Form.Item>
        <Row gutter={[12, 12]}>
          <Col xs={24} sm={24} md={24} lg={8} xl={8}>
            <Form.Item
              label="Harga normal"
              name="price"
              rules={[{ required: true, message: "Stok produk wajib diisi" }]}
              hasFeedback
            >
              <Input type={"number"} />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={8} xl={8}>
            <Form.Item label="Diskon" name="price_disc">
              <Input
                type={"number"}
                suffix="%"
                onChange={(e) => {
                  const dirtyForm = form.getFieldsValue();
                  dirtyForm.price_disc = e.target.value;
                  dirtyForm.price_total =
                    dirtyForm.price * (dirtyForm.price_disc / 100);
                  form.setFieldsValue(dirtyForm);
                }}
              />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={8} xl={8}>
            <Form.Item label="Harga diskon" name="price_total">
              <Input type={"number"} disabled />
            </Form.Item>
          </Col>
        </Row>
        <div style={{ float: "right", marginBottom: 20 }}>
          <Button
            type="link"
            onClick={() => history.goBack()}
            style={{ marginRight: 10 }}
          >
            Batal
          </Button>
          <Button type="primary" htmlType="submit">
            Simpan
          </Button>
        </div>
      </div>
    </Form>
  );
}
export default ProductForm;
