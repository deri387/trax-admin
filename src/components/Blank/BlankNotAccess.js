import Result from "antd/es/result";
import Button from "antd/es/button";
import history from "../../utils/history";
function BlankNotAccess() {
  return (
    <Result
      status="403"
      title="403"
      subTitle="Mohon maaf, Anda tidak dapat mengakses halaman ini"
      extra={
        <Button type="primary" onClick={() => history.goBack()}>
          Kembali
        </Button>
      }
    />
  );
}
export default BlankNotAccess;
