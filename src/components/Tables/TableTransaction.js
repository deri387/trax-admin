import Table from "antd/es/table";
import Card from "antd/es/card";
import DatePicker from "antd/es/date-picker";
import Button from "antd/es/button";
import { useEffect, useState } from "react";
import http from "../../utils/http";
import helper from "../../utils/helper";

const columns = () => [
  {
    title: "Pembeli",
    dataIndex: "name",
    render: (text, record) => `${record.user?.name} (${record.user?.name})`,
  },
  {
    title: "Produk",
    dataIndex: "product",
    render: (text, record) => record.product?.name,
  },
  {
    title: "Info Akun",
    dataIndex: "info",
    render: (text, record) =>
      record.account + "(" + record.zone + ") - " + record.nickname,
  },
  {
    title: "Total",
    dataIndex: "total",
    render: (text, record) => "Rp." + helper.formatRupiah(record.total),
  },
];

function TableTransaction(props) {
  const [data, setData] = useState([]);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 10,
    total: 0,
  });
  const [loading, setLoading] = useState(false);
  const { RangePicker } = DatePicker;
  useEffect(() => {
    fetch("", "", pagination);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetch = (start_date = startDate, end_date = endDate, pagination) => {
    setLoading(true);
    http
      .get(
        `/v1/transaction?start_date=${start_date}&end_date=${end_date}&page=${
          pagination.current
        }&limit=${props.action ? 10 : 5}`
      )
      .then(async (data) => {
        await setLoading(false);
        await setData(data?.data?.serve.data);
        await setPagination({
          current: data?.data?.serve.current_page,
          pageSize: data?.data?.serve.per_page,
          total: data?.data?.serve.total,
        });
      });
  };

  return (
    <>
      {props.action ? (
        <Card className="flex align-center card-product-filter">
          <RangePicker
            onChange={(e, f) => {
              if (f.length > 0) {
                setStartDate(f[0]);
                setEndDate(f[1]);
              }
            }}
          />
          <Button
            type="primary"
            onClick={() => {
              fetch(startDate, endDate, pagination);
            }}
            style={{ marginLeft: 10 }}
          >
            Filter
          </Button>
        </Card>
      ) : null}
      <Table
        scroll={{ y: 300 }}
        style={{ marginTop: 10 }}
        columns={columns()}
        rowKey={(record) => record.id}
        dataSource={data}
        pagination={props.action ? pagination : false}
        hide
        loading={loading}
      />
    </>
  );
}

export default TableTransaction;
