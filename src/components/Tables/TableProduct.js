import Table from "antd/es/table";
import Button from "antd/es/button";
import Input from "antd/es/input";
import Card from "antd/es/card";
import { PlusOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import http from "../../utils/http";
import history from "../../utils/history";
import helper from "../../utils/helper";
const columns = () => [
  {
    title: "Nama",
    dataIndex: "name",
  },
  {
    title: "Stok",
    dataIndex: "stock",
  },
  {
    title: "Harga",
    dataIndex: "price_total",
    render: (text, record) =>
      "Rp." +
      helper.formatRupiah(record.price) +
      (record.price_disc > 0
        ? " OFF " +
          record.price_disc +
          "%" +
          "( Rp." +
          helper.formatRupiah(record.price_total) +
          " )"
        : ""),
  },
];

function TableProduct() {
  const [data, setData] = useState([]);
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 10,
    total: 0,
  });
  const [loading, setLoading] = useState(false);
  const { Search } = Input;

  useEffect(() => {
    fetch("", pagination);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleTableChange = async (page, filters, sorter) => {
    fetch("", page);
  };

  const fetch = (data, pagination) => {
    setLoading(true);
    http
      .get(`/v1/product?page=${pagination.current}&name=${data}`)
      .then(async (data) => {
        await setLoading(false);
        await setData(data?.data?.serve.data);
        await setPagination({
          current: data?.data?.serve.current_page,
          pageSize: data?.data?.serve.per_page,
          total: data?.data?.serve.total,
        });
      });
  };

  return (
    <>
      <Card className="flex align-center card-product-filter">
        <Search
          placeholder="Cari data"
          onSearch={(e) => fetch(e, pagination)}
          style={{ width: "30vh" }}
        />
        <div style={{ marginLeft: "auto" }} className="flex align-center">
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => history.push("/add-product")}
            style={{ marginRight: 10 }}
          >
            Buat baru
          </Button>
        </div>
      </Card>
      <Table
        scroll={{ y: 300 }}
        style={{ marginTop: 10 }}
        columns={columns()}
        rowKey={(record) => record.id}
        dataSource={data}
        pagination={pagination}
        loading={loading}
        onChange={handleTableChange}
        onRow={(record, rowIndex) => {
          return {
            onClick: (event) => history.push("/edit-product/" + record.id),
          };
        }}
      />
    </>
  );
}

export default TableProduct;
