import React, { lazy, Suspense } from "react";
import { Route, Switch, withRouter, Redirect } from "react-router-dom";
import helper from "../utils/helper";

const LoginPage = lazy(() => import("../pages/LoginPage"));
const DashboardPage = lazy(() => import("../pages/DashboardPage"));
const ProductListPage = lazy(() => import("../pages/ProductListPage"));
const ProductAddPage = lazy(() => import("../pages/ProductAddPage"));
const ProductEditPage = lazy(() => import("../pages/ProductEditPage"));
const TransactionListPage = lazy(() => import("../pages/TransactionListPage"));
const BlankNotAccess = lazy(() => import("../components/Blank/BlankNotAccess"));

const PageLoader = () => (
  <div id="loader">
    <div className="loadingio-spinner-rolling-3kjtqyynjid">
      <div className="ldio-ucwibazh2i9">
        <div></div>
      </div>
    </div>
  </div>
);
const PublicRoute = ({ children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        !helper.isAuthenticated() ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/dashboard",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

const PrivateRoute = ({ title, children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        helper.isAuthenticated() ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

function Router() {
  return (
    <Suspense fallback={<PageLoader />}>
      <Switch>
        <Route exact path="/" render={() => <Redirect to="/dashboard" />} />
        <PublicRoute path="/login">
          <LoginPage />
        </PublicRoute>
        <PrivateRoute path="/dashboard">
          <DashboardPage />
        </PrivateRoute>
        <PrivateRoute path={`/product`}>
          {helper.isAuthenticated() ? <ProductListPage /> : <BlankNotAccess />}
        </PrivateRoute>
        <PrivateRoute path={`/add-product`}>
          {helper.isAuthenticated() ? <ProductAddPage /> : <BlankNotAccess />}
        </PrivateRoute>
        <Route
          exact
          path={`/edit-product/:id?`}
          render={(props) =>
            helper.isAuthenticated() ? (
              <ProductEditPage id={props.match.params.id} />
            ) : (
              <Redirect
                to={{
                  pathname: "/login",
                  state: { from: props.location },
                }}
              />
            )
          }
        />
        <PrivateRoute path={`/transaction`}>
          {helper.isAuthenticated() ? (
            <TransactionListPage />
          ) : (
            <BlankNotAccess />
          )}
        </PrivateRoute>
      </Switch>
    </Suspense>
  );
}
export default withRouter(Router);
